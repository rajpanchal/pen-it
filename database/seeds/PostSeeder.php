<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Tag;
use App\Post;
use Carbon\Carbon;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name'=>'News']);
        $categoryDesign = Category::create(['name'=>'Design']);
        $categoryTechnology = Category::create(['name'=>'Technology']);
        $categoryEngineering = Category::create(['name'=>'Engineering']);

        $tagCustomers = Tag::Create(['name'=>'customers']);
        $tagDesigns = Tag::Create(['name'=>'design']);
        $tagLaravel = Tag::Create(['name'=>'laravel']);
        $tagCoding = Tag::Create(['name'=>'coding']);

        $post1 = Post::create([
            'title'=>'We relocated our office to HOME!',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10, 18)),
            'content'=>Faker\Factory::create()->paragraph(rand(3, 7), true),
            'image'=>'posts/1.jpg',
            'category_id'=>$categoryNews->id,
            'user_id'=>2,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);

        $post2 = Post::create([
            'title'=>'Google Assistant May Soon Make Its Way To Fitbit Wearables',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10, 18)),
            'content'=>Faker\Factory::create()->paragraph(rand(3, 7), true),
            'image'=>'posts/2.jpg',
            'category_id'=>$categoryTechnology->id,
            'user_id'=>2,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);

        $post3 = Post::create([
            'title'=>'Adobe\'s Photoshop Camera Lets You Apply AI-Filters To Photos In Real Time',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10, 18)),
            'content'=>Faker\Factory::create()->paragraph(rand(3, 7), true),
            'image'=>'posts/3.jpg',
            'category_id'=>$categoryDesign->id,
            'user_id'=>1,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);

        $post4 = Post::create([
            'title'=>'Meet These Indian Coders As Young As Six Who Are Shaping Up A Powerful Digital Future',
            'excerpt'=>Faker\Factory::create()->sentence(rand(10, 18)),
            'content'=>Faker\Factory::create()->paragraph(rand(3, 7), true),
            'image'=>'posts/4.jpg',
            'category_id'=>$categoryEngineering->id,
            'user_id'=>3,
            'published_at'=>Carbon::now()->format('Y-m-d')
        ]);

        $post1->tags()->attach([$tagCustomers->id]);
        $post2->tags()->attach([$tagCoding->id, $tagCustomers->id]);
        $post3->tags()->attach([$tagDesigns->id, $tagLaravel->id]);
        $post4->tags()->attach([$tagCoding->id, $tagDesigns->id, $tagLaravel->id, $tagCustomers->id]);

    }
}


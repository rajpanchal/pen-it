<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'rajpanchal.3786@gmail.com')->get()->first();
        if(!$user){
            User::create([
                'name'=>'Raj Panchal',
                'email'=>'rajpanchal.3786@gmail.com',
                'password'=>Hash::make('abcd1234'),
                'role'=>'admin'
            ]);
        }else{
            $user->update(['role'=>'admin']);
        }

        User::create([
            'name'=>'Chintan Gohil',
            'email'=>'chintangohil2000@gmail.com',
            'password'=>Hash::make('abcd1234'),
        ]);

        User::create([
            'name'=>'Jash Doshi',
            'email'=>'jashdoshi27@gmail.com',
            'password'=>Hash::make('abcd1234'),
        ]);
    }
}
